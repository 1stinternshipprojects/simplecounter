import React, {Component} from 'react'
import {View,Text,TouchableOpacity,StyleSheet} from 'react-native'

class IncrementButton extends Component {
    render(){
        return(
            <View>
                <TouchableOpacity 
                    onPress={this.props.click}
                    style={styles.button}
                    >
                    <Text>+</Text>
                </TouchableOpacity> 
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
      }
})

export default IncrementButton