import React,{Component} from 'react'
import {Text,StyleSheet,View,TouchableOpacity} from 'react-native'

class DecrementButton extends Component{
    render(){
        return(
            <View>
                <TouchableOpacity 
                    onPress={this.props.click}
                    style={styles.button}
                    >
                    <Text>-</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
    }
})

export default DecrementButton