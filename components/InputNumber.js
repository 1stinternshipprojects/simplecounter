import React, {Component} from 'react'
import {View,TextInput,StyleSheet} from 'react-native'

class InputNumber extends Component {
    render(){
        return(
            <View>
                 <TextInput
                    onChangeText={this.props.changedText}
                    value={this.props.value}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={this.props.placeholderTextColor}
                    />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputNumberContainer: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    }
})

export default InputNumber