import React, {Component} from 'react';
import {StyleSheet,View} from 'react-native';
import IncrementButton from './components/IncrementButton'
import DecrementButton from './components/DecrementButton'
import Dynamicnumber from './components/Dynamicnumber'
import InputNumber from './components/InputNumber'

class App extends Component {

  state = {
    count : 0
  }

  incrementHnadler = () => {
    this.setState({ count: this.state.count+1 })
  }

  decrementHnadler = () => {
    this.setState({ count: this.state.count-1 })
  }



  render(){
    return(
      <View style={styles.container}>
        <View>
          <InputNumber 
            placeholder={'enter number'}
            placeholderTextColor={'black'}
            value={this.state.count}
            changedText= {(count) => this.setState({count})}
            />
          <Dynamicnumber count={this.state.count}/>
          <View style={styles.buttons}>
            <IncrementButton click={this.incrementHnadler}/>
            <DecrementButton click={this.decrementHnadler}/>
          </View>
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
    container :{
      // flexDirection: 'column'
    },
    buttons: {
      // flexDirection : 'row'
    }
  
});

export default App;
